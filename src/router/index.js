import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from "../pages/Home";
import Find from "../pages/Find";
import Order from "../pages/Order";
import Profile from "../pages/Profile";
import Login from "../pages/Login";
import NavBottom from "../components/NavBottom";

Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      component: NavBottom,
      redirect: '/home',
      children: [
        {
          path: '/home',
          component: Home
        },
        {
          path: '/find',
          component: Find
        },
        {
          path: '/order',
          component: Order
        },
        {
          path: '/profile',
          component: Profile
        },
      ]
    },
    {
      path: '/login',
      component: Login
    }
  ]
})
