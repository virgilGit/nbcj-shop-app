// 导入
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from "axios";
import { Form, Field, CellGroup, Button, Notify} from 'vant';
import 'vant/lib/index.css'

// 配置 this.$router.push
Vue.config.productionTip = false
Vue.prototype.$axios=axios
Vue.use(Form);
Vue.use(Field);
Vue.use(CellGroup);
Vue.use(Button)
Vue.use(Notify)
axios.interceptors.response.use(
  res => {
    // 拦截所有的请求的响应
    // 响应的数据会被抓到放到res中
    if (res.data.code === 403) {
      // 403是后端的状态码，表示用户未登录
      router.push('/login')
    }

    return Promise.resolve(res)
  }
)
axios.interceptors.request.use(config => {

  var token = sessionStorage.getItem("Authorization")
  if (token) {
    config.headers['Authorization'] = token;
  }

  return config;
})

// vue对象实例化
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
